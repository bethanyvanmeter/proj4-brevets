"""
Nose tests for acp_times.py

"""
import acp_times

import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)


def test_normal_200():
	'''
	A standard checkpoint of 100 in a 200 brevit

	brevit distance = 200
	checkpoint km = 100
	'''
	assert acp_times.open_time(100, 200, "2017-01-01T00:00:00+00:00") == "2017-01-01T02:56:00+00:00"
	assert acp_times.close_time(100, 200, "2017-01-01T00:00:00+00:00") == "2017-01-01T06:40:00+00:00"

def test_lower_bracket_use_km():
	'''
	A checkpoint at 200 of a 400 brevit should use the bracket 0-200 speeds
	rather than the bracket 200-400 speeds

	brevit distance = 400
	checkpoint km = 200
	'''
	assert acp_times.open_time(200, 400, "2017-01-01T00:00:00+00:00") == "2017-01-01T05:53:00+00:00"
	assert acp_times.close_time(200, 400, "2017-01-01T00:00:00+00:00") == "2017-01-01T13:20:00+00:00"


def test_limit_600():
	'''
	A checkpoint at a brevit distance limit

	brevit distance = 600
	checkpoint km = 600
	'''
	assert acp_times.open_time(600, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T18:48:00+00:00"
	assert acp_times.close_time(600, 600, "2017-01-01T00:00:00+00:00") == "2017-01-02T16:00:00+00:00"

def test_zero_km():
	'''
	Leaving an hour buffer to leave the start point

	brevit distance = 200
	checkpoint km = 0
	'''
	assert acp_times.open_time(0, 200, "2017-01-01T00:00:00+00:00") == "2017-01-01T00:00:00+00:00"
	assert acp_times.close_time(0, 200, "2017-01-01T00:00:00+00:00") == "2017-01-01T01:00:00+00:00"

def test_over_brevit_km():
	'''
	A checkpoint over the brevit distance

	brevit distance = 400
	checkpoint km = 410
	'''
	assert acp_times.open_time(410, 400, "2017-01-01T00:00:00+00:00") == "2017-01-01T12:08:00+00:00"
	assert acp_times.close_time(410, 400, "2017-01-01T00:00:00+00:00") == "2017-01-02T03:00:00+00:00"

def test_over_brevit_km():
	'''
	The first 60km being calculated at 20km/hr

	brevit distance = 400
	checkpoint km = 50
	'''
	assert acp_times.open_time(50, 400, "2017-01-01T00:00:00+00:00") == "2017-01-01T01:28:00+00:00"
	assert acp_times.close_time(50, 400, "2017-01-01T00:00:00+00:00") == "2017-01-01T03:30:00+00:00"

def test_change_in_speed_600():
	'''
	Many checkpoints at varying distances over the brevit distance

	Taken from example 2 here: https://rusa.org/pages/acp-brevet-control-times-calculator
	brevit distance = 600
	checkpoint distances = 0,50,100,150,200,250,300,350,400,450,500,550,609
	'''
	assert acp_times.open_time(0, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T00:00:00+00:00"
	assert acp_times.close_time(0, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T01:00:00+00:00"

	assert acp_times.open_time(50, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T01:28:00+00:00"
	assert acp_times.close_time(50, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T03:30:00+00:00"

	assert acp_times.open_time(100, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T02:56:00+00:00"
	assert acp_times.close_time(100, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T06:40:00+00:00"

	assert acp_times.open_time(150, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T04:25:00+00:00"
	assert acp_times.close_time(150, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T10:00:00+00:00"

	assert acp_times.open_time(200, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T05:53:00+00:00"
	assert acp_times.close_time(200, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T13:20:00+00:00"

	assert acp_times.open_time(250, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T07:27:00+00:00"
	assert acp_times.close_time(250, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T16:40:00+00:00"

	assert acp_times.open_time(300, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T09:01:00+00:00"
	assert acp_times.close_time(300, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T20:00:00+00:00"

	assert acp_times.open_time(350, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T10:34:00+00:00"
	assert acp_times.close_time(350, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T23:20:00+00:00"

	assert acp_times.open_time(400, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T12:08:00+00:00"
	assert acp_times.close_time(400, 600, "2017-01-01T00:00:00+00:00") == "2017-01-02T02:40:00+00:00"

	assert acp_times.open_time(450, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T13:48:00+00:00"
	assert acp_times.close_time(450, 600, "2017-01-01T00:00:00+00:00") == "2017-01-02T06:00:00+00:00"

	assert acp_times.open_time(500, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T15:28:00+00:00"
	assert acp_times.close_time(500, 600, "2017-01-01T00:00:00+00:00") == "2017-01-02T09:20:00+00:00"

	assert acp_times.open_time(550, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T17:08:00+00:00"
	assert acp_times.close_time(550, 600, "2017-01-01T00:00:00+00:00") == "2017-01-02T12:40:00+00:00"

	assert acp_times.open_time(609, 600, "2017-01-01T00:00:00+00:00") == "2017-01-01T18:48:00+00:00"
	assert acp_times.close_time(609, 600, "2017-01-01T00:00:00+00:00") == "2017-01-02T16:00:00+00:00"